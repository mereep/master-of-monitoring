/**
 * Description:
 * Module declaration AngularJS
 * 
 * This file is part of submission 
 * @author Richard Vogel <webdes87@gmail.com>
 */
(function(undefined){
'use strict';
window.angular.module('mom', ['helper']);
})();
