/**
 * Description:
 * Main controller for machine management
 *
 * This file is part of submission
 * @author Richard Vogel <webdes87@gmail.com>
 */

(function (undefined) {
'use strict';
window.angular.module('mom')

.controller('MachineCtrl',
        ['httpService', '$scope', 'RpcService', 'dialogs', 'translator',
function (httpService,   $scope,   rpcService,   dialogs,   translator) {
    /**
     * Stores url to server
     * @type {string}
     */
    var serverAddress = 'http://127.0.0.1:81';

    $scope.currentUser = "admin";
    $scope.devices = [];

    /**
     * Only allow rebooting if we are not already rebooting
     * @param {Array} device
     * @return {boolean}
     */
    $scope.isRebootAllowed = function (device) {
        return device.state !== 'rebooting';    
    };
    
    $scope.reboot = function (device) {
        httpService.postData(
            serverAddress,
            rpcService.createCall('reboot', {'deviceIp':device.ip, 'user': $scope.currentUser}),
            function success() {
                getDeviceData(true);
            },
            function error(msg, data) {
                dialogs.showOkDialog(
                    dialogs.TYPE_ERROR,
                    'Anfragefehler',
                    'Rebooten fehlgeschlagen'+': '+msg
                );
            }
        );
    };

    /**
     * Turns container on / off
     *
     * @param {string} deviceIp
     * @param {string} containerName
     */
    $scope.toggleContainerState = function (deviceIp, containerName) {
        if (checkDeviceExistent(deviceIp, containerName, true)) {
            // Gather data
            var device = getDeviceByIp(deviceIp),
                container = getContainerByName(device, containerName);

            // Choose correct command to send
            var cmd = 'start_container';
            if(container.state === 'on') {
                cmd = 'stop_container'
            }

            // ... and send
            httpService.postData(serverAddress, rpcService.createCall(
                    cmd,
                    {'deviceIp': deviceIp, 'containerName': containerName, 'user': $scope.currentUser}
                ),
                function success(msg, data) {
                    getDeviceData(true);
                    dialogs.showOkDialog(
                        dialogs.TYPE_SUCCESS,
                        translator.translate('Erfolg'),
                        translator.translate('Container status erfolgreich geändert')
                    )
                },
                function error(msg, data) {
                    dialogs.showOkDialog(
                        dialogs.TYPE_ERROR,
                        translator.translate('Fehler'),
                        translator.translate('Containerstatus konnte nicht geändert werden')+':'+msg
                    )
                }
            )
        }
    };

    /**
     * Adds a new device
     */
    $scope.addDevice = function () {
        dialogs.showInputDialog(translator.translate('IP der Maschine: '), '', function (deviceIp) {
            // User cancelled -> dont show second dialog
            if(!deviceIp) return;
            dialogs.showInputDialog(translator.translate('Name der Maschine: '), '', function (name) {
                if(!name) return;
                httpService.postData(
                    serverAddress,
                    rpcService.createCall(
                        'add_device',
                        {
                            'deviceIp': deviceIp, 'name': name, 'user': $scope.currentUser}
                        ),
                    function success(msg, data) {
                        dialogs.showOkDialog(
                            dialogs.TYPE_SUCCESS,
                            translator.translate('Erfolg'),
                            translator.translate('Maschine erfolgreich hinzugefügt')
                        );
                    },
                    function error(msg, data) {
                        dialogs.showOkDialog(
                            dialogs.TYPE_ERROR,
                            translator.translate('Fehler'),
                            translator.translate('Containerstatus konnte nicht geändert werden') + ': ' + msg
                        );
                    }
                );
            })
        });
    };
    /**
     * Gets the time in local format (Does not work in some browsers, though)
     *
     * @param timeStr
     * @return string
     */
    $scope.getTime = function(timeStr) {
       return new Date(timeStr).toLocaleString();
    };

    /**
     * Sends delete request to server
     *
     * @param {string} deviceIp
     * @param {string} containerName
     */
    $scope.deleteContainer = function (deviceIp, containerName) {
        if (checkDeviceExistent(deviceIp, containerName, true)) {
            httpService.postData(
                serverAddress,
                rpcService.createCall(
                    'delete_container',
                    {'deviceIp': deviceIp, 'containerName': containerName, 'user': $scope.currentUser}
                    ),
                function success(msg, data) {
                    getDeviceData(true);
                    dialogs.showOkDialog(
                        dialogs.TYPE_SUCCESS,
                        translator.translate('Erfolg'),
                        translator.translate('Container wurde gelöscht')
                    )
                },
                function error(msg, data) {
                    dialogs.showOkDialog(
                        dialogs.TYPE_ERROR,
                        translator.translate('Fehler'),
                        translator.translate('Fehler beim Löschen des Containers') +': '+msg
                    )
                }
            )
        }
    };

    /**
     *
     * @param deviceIp
     */
    $scope.newContainer = function (deviceIp) {
        dialogs.showInputDialog(translator.translate('Name des Containers: '), '', function (containerName) {
            if(containerName) {
                // Check if device is still there and not async killed
                var device = getDeviceByIp(deviceIp);
                if(device) {
                    httpService.postData(
                        serverAddress,
                        rpcService.createCall(
                            'install_container',
                            {'ip': deviceIp, 'name': containerName, 'user': $scope.currentUser}
                            ),
                        function success(msg, data) {
                            getDeviceData(true);
                            dialogs.showOkDialog(
                                dialogs.TYPE_SUCCESS,
                                translator.translate('Container successfully created'),
                                msg);
                        }, function error(msg, data) {
                            dialogs.showOkDialog(
                                dialogs.TYPE_ERROR,
                                translator.translate('Error creating container'), msg
                            );
                        }
                    )
                }
            }
        })
    };
    
    /**
     * Fetches device information from server
     */
    function getDeviceData(noMessage) {
        httpService.postData
        (serverAddress,
            rpcService.createCall('get_devices'),
            function success(msg, data) {
            //$scope.devices = angular.extend($scope.devices, data.devices);
            $scope.devices = data.devices;
        }, function error() {
            if(!noMessage) {
                dialogs.showOkDialog(
                    dialogs.TYPE_ERROR,
                    'Anfragefehler',
                    'Abfragen der Device Daten fehlgeschlagen'
                );
            }
        });
    }

    /**
     * Gets the device by ip
     *
     * @param ip
     * @return {Array} the device (undefined if not found)
     */
    function getDeviceByIp(ip) {
        return _.find($scope.devices, function (device) {
            return device.ip === ip;
        })
    }


    /**
     * Finds device by container
     *
     * @param {Array} device
     * @param containerName
     * @return {Object|undefined}
     */
    function getContainerByName(device, containerName) {
        return _.find(device.containers, function (checkDevice) {
            return checkDevice.name === containerName;
        })
    }

    var watcherFunction = window.setInterval(function () {
        $scope.$apply(function () {getDeviceData(true)});
    }, 2000);

    /**
     * Checks if container given, spits out a message on failure to user if requested
     *
     * @param deviceIp
     * @param containerName
     * @param informUser
     * @return {boolean}
     */
    function checkDeviceExistent(deviceIp, containerName, informUser) {
        var device = getDeviceByIp(deviceIp);
        if (device) {
            var container = getContainerByName(device, containerName);
            if (container) {
                return true;
            } else {
                if (informUser) dialogs.showErrorDialog(dialogs.ERROR, translator.translate('Fehler'), translator.translate('Container nicht mehr verfügbar'))
            }
        } else {
            if (informUser) dialogs.showOkDialog(dialogs.ERROR, translator.translate('Fehler'), translator.translate('Gerät nicht mehr verfügbar'))
        }

        return false;
    }
    /**
     * Initializes controller
     */
    function init() {
        $scope.devices = [];
        getDeviceData();
    }

    init();
}]
);
})(undefined);

