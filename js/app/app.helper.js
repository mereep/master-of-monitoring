/**
 * Description:
 * Some Services or other helpers
 *
 * This file is part of submission
 * @author Richard Vogel <webdes87@gmail.com>
 */
(function (undefined) {
'use strict';
window.angular.module('helper', [])

/**
 * Simple translation helper
 */
.service('translator', function () {
    /**
     * just echos the string, not very useful now :D
     * 
     * @param string string
     * @param {Array} params
     * @returns string
     */
    this.translate = function (string, params) {
        return string;
    }
})
/**
 * Servicce for working with requests
 */
.service('httpService', ['$http', 'translator',function ($http, translator) {
    
/**
 * Called when a ajax response is coming back (nevertheless of error or success)
 *
 * @param response
 */
function handleResponse (response) {
    // Do nothing now
}

/**
 * Post data to server
 *
 * @param url
 * @param data
 * @param successCb
 * @param errorCb
 */
this.postData = function (url, data, successCb, errorCb) {
    var oldType = $http.defaults.headers.post["Content-Type"];
    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

    $http({
            method: 'POST',
            url: url,
            data: data
        }
    ).then(function success(response) {
        $http.defaults.headers.post["Content-Type"] = oldType;
        handleResponse(response);
        if (angular.isFunction(successCb)) {
            if(response.data && response.data.result) {
                var msg = response.data.result.msg,
                    data = response.data.result.data;
            } else {
                var data = undefined,
                    msg = translator.translate('Unknown error');
            }

            successCb.call(response, response.data.result.msg, response.data.result.data);
        }
    }, function error(response) {
        $http.defaults.headers.post["Content-Type"] = oldType;
        handleResponse(response);

        if (angular.isFunction(errorCb)) {
            if(response.data && response.data.result) {
                var msg = response.data.result.msg,
                    data = response.data.result.data;
            } else {
                var error = response.data && response.data.error ? response.data.error : translator.translate('Unknown error');
                var data = undefined,
                    msg = error
                    ;
            }
            errorCb.call(response, msg, data);
        }
    })
};
}])
/** Can show some dialog boxes*/
.service('dialogs', ['translator', function(translator) {

    this.TYPE_WARNING = 'warning';
    this.TYPE_ERROR = 'error';
    this.TYPE_INFO = 'info';
    this.TYPE_SUCCESS = 'success';

    var self = this;

    /**
     * Get font awesome classes for dialog types
     *
     * @param type
     * @returns {*}
     */
    function getIconCssClass(type) {
        switch (type) {
            case self.TYPE_ERROR:
                return 'fa fa-frown-o icon-error text-danger';

            case self.TYPE_WARNING:
                return 'fa fa-exclamation-triangle icon-warning text-warning';

            case self.TYPE_INFO:
                return 'fa fa-info icon-warning text-primary';

            case self.TYPE_SUCCESS:
                return 'fa fa-check icon-success text-success';

        }
    }

    /**
     * Shows a common dialog with ok btn
     */
    this.showOkDialog = function(type, title, text, cbOk){
        type = type || self.TYPE_INFO;
        text = text || '';
        title = title || '';

        text =
            '<i style="float: left; margin-right: 2em; margin-bottom: 1em; font-size: 4em" class="'+getIconCssClass(type)+'"></i>' +
            '<div style="min-height: 5em">'+text+'</div>';
        if (typeof cbOk !== 'function') {
            cbOk = function () {}
        }

        bootbox.dialog({
            message: text,
            title: title,

            buttons: {
                main: {
                    label: translator.translate('Ok'),
                    className: 'btn-primary',
                    callback: cbOk
                }
            }

        })
    };


    /**
     * Shows a dialog having a input box
     *
     * @param {string} text
     * @param {string} defaultVal
     * @param {function} cbOk
     */
    this.showInputDialog = function (text, defaultVal, cbOk) {
        if(typeof cbOk !== 'function') {
            console.error('showInput dialog must have a callbacl set, otherwise value will be lost (Code: 3298209)');
        }
        defaultVal = defaultVal ? defaultVal : '';
        text = text ? text : translator.translate('Your input') + ': ';

        bootbox.prompt({'title': text, value: defaultVal, callback: function (val) {
            cbOk.call(null, val);
        }});
    }


}])
/**
 * RPC helper
 */
.service('RpcService', function() {
var callNr = 0;
/**
 * Creates jsonp
 * @param cmd
 * @param params
 */
this.createCall = function (cmd, params) {
    return {
        'jsonrpc':'2.0',
        'id':++callNr,
        'method':cmd,
        'params': params ? params : {}
    }
}
})
})(undefined);

