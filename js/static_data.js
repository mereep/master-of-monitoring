/**
 * Description:
 * Some static data for server
 *
 * This file is part of submission
 * @author Richard Vogel <webdes87@gmail.com>
 */

'use strict';
var states = {
    container_off: 'off',
        container_on: 'on',
        device_online: 'online',
        device_offline: 'offline',
        device_rebooting: 'rebooting'
};

module.exports = {
    users: {'user1': 'password1', 'user2': 'password2'},
    states: states,
    adminUsers: ['admin', 'mereep','the_one_who_pwned_us'], // Only these users can access certain functionality
    allowedMethods: ['get_devices', 'get_containers'], // These methods can be called by non admins
    // our "Database" ;)
    startContainers: [
        {
            name: 'Gerät 1',
            startTime: new Date(),
            startedBy: 'admin',
            state: states.device_online,
            ip: '1.2.3.4',
            containers: [
                {
                    name: 'Container 1_1',
                    startedBy: 'admin',
                    state: states.container_on,
                    startTime: new Date()
                },
                {
                    name: 'Container 1_2',
                    startedBy: 'SomeOtherUser',
                    state: states.container_off,
                    startTime: new Date()
                }
            ]
        },
        {
            name: 'Gerät 2',
            startTime: new Date(),
            startedBy: 'admin',
            state: states.device_offline,
            ip: '1.2.3.5',
            containers: [
                {
                    name: 'Container 1_2',
                    startedBy: 'admin',
                    state: states.container_off,
                    startTime: new Date()
                }
            ]
        }
    ]
};