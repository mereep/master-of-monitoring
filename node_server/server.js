/**
 * Node Server part of submission for Coding competition <webdes87@gmail.com>
 */


/** @var deviceCtrl DeviceCtrl */ 
var http = require("http"),
    RPCHandler = require("jsonrpc").RPCHandler,
    deviceCtrl = require('./device_controller'),
    staticData = require('../js/static_data'),
    _ = require('underscore')
    ;


var requireValidUser = true // Turn off for no auth
;

// Some startup data for machines


// Change behaviour of RPC module to intercept the
// request and do some stuff BEFORE executing actually
// This should be available by library already
// @TODO switch library since this code may break eventually on updated of Lib
RPCHandler.prototype._processRequest = function () {
    var self = this;

    var _CONTENT = '';

    // Receive data
    this.HTTPRequest.addListener('data', function(chunk){
        _CONTENT+= chunk;
    });

    /**
     * Called when HTTP Request is Read completely by NodeJS
     */
    var dataCompleteCallback =  function() {
        console.log("Data:",_CONTENT);
        var data = JSON.parse(_CONTENT),
            user = data && data['params'] && data['params']['user'] ? data['params']['user'] : ''
            ;

        // Check for user permission
        if (data && typeof data['method'] !== 'undefined' && canExecute(data['method'], user)) {
            console.info('Authorized request');
            // Prevent loop
            self.HTTPRequest.removeListener('end', dataCompleteCallback);

            // Fire original listener
            self._post_handler.apply(self);
            self.HTTPRequest.emit('data', _CONTENT);
            self.HTTPRequest.emit('end');

        } else {
            // Not authorized, go out
            console.warn('Unauthorized request');
            makeErrorAnswer(self, 'You are not allowed to do this (Code: 349350435345)');
        }
    };

    this.HTTPRequest.addListener('end', dataCompleteCallback);

};

// ########################### start server ################################
console.log('listening on port 80');
http.createServer(function (request, response) {
    if(request.method == "POST"){
        // if POST request, handle RPC
        console.log("Incoming request");


        // Some browsers will make trouble w/o this
        response.setHeader('Access-Control-Allow-Origin','*');

        new RPCHandler(request, response, RPCMethods, true);
    }else{
        // if GET request response with greeting
        response.end("This should be called via POST Request");
    }
}).listen(81);

/**
 * Sets the appropriate data in response object
 *
 * @param rpc
 * @param msg
 * @param data
 * @return {*}
 */
function makeSuccessAnswer (rpc, msg, data) {
    data = data ? data : {};
    msg = msg ? msg : '';
    rpc.response({'status':'success','data': data, 'msg':msg});
    return rpc;
}

/**
 * Checks if we are allowed to executed a command
 *
 * @param requestMethodName
 * @return {boolean}
 */
function canExecute (requestMethodName, user) {
    return !requireValidUser ||
        _.contains(staticData.allowedMethods, requestMethodName) ||
        _.contains(staticData.adminUsers, user)
        ;
}


/**
 * Sets the appropriate data in response object
 * @param rpc
 * @param msg
 * @param data
 * @return {*}
 */
function makeErrorAnswer (rpc, msg) {
    msg = msg ? msg : '';
    rpc.error(msg);
    return rpc;
}

// Available RPC methods
RPCMethods = {
    get_devices: function (rpc) {
        // At this point we should check if some status change at all happened
        // for that specific client (and / or split into fine granular requests)
        // omitted for simplicity since amount of data is less ;)
        var result = {'devices': deviceCtrl.getAllDevices()};
        makeSuccessAnswer(rpc, 'success', result);
    },
    // Reboot a device
    reboot: function (rpc, params) {
        if(params['deviceIp']) {
            if (deviceCtrl.rebootDevice(params['deviceIp'], params['user'])) {
                makeSuccessAnswer(rpc, 'Reboot initiated (Code: 492342)');
            } else {
                makeErrorAnswer(rpc, 'Reboot failed (Code: 039423)');
            }
        } else {
            makeErrorAnswer(rpc, 'No device ip specified (Code: 32435)');
        }
    },
    install_container: function (rpc, params) {
        if(params['ip'] && typeof params['ip'] === 'string' &&
           params['name'] && typeof params['name'] === 'string') {
            if(deviceCtrl.addContainer(params['ip'], params['name'], params['user'])) {
                makeSuccessAnswer(rpc, 'Device successfully added (Code: 948230948)');
            } else {
                makeErrorAnswer(rpc, 'Could not add container (Name already in use?) (Code: 456456456)');
            }
        } else {
            makeErrorAnswer(rpc, 'No device ip or no name specified (Code: 9823402)');
        }
    },
    delete_container: function (rpc, params) {
        if(params['deviceIp'] && typeof params['deviceIp'] === 'string' &&
            params['containerName'] && typeof params['containerName'] === 'string') {
            if (deviceCtrl.deleteContainer(params['deviceIp'], params['containerName'])) {
                makeSuccessAnswer(rpc, 'Successfully deleted container (Code: 9328345)');
            } else {
                makeErrorAnswer(rpc, 'Could not delete container (Code: 567567567)');

            }
        } else {
            makeErrorAnswer(rpc, 'No device ip or or no container name specified (Code: 45645646)');

        }
    },
    get_containers: function () {
        makeSuccessAnswer(rpc, "success", {containers: deviceCtrl.getAllContainers()});
    }
    ,
    stop_container: function(rpc, params) {
        // Will stop a container
        if(params['deviceIp'] && typeof params['deviceIp'] === 'string' &&
            params['containerName'] && typeof params['containerName'] === 'string') {
            var device = deviceCtrl.getDeviceByIp(params['deviceIp']),
                container = null;

            if (device && (container = deviceCtrl.getContainerByName(device, params['containerName']))) {
                if (deviceCtrl.stopContainer(container, params['user'])) {
                    makeSuccessAnswer(rpc, 'Successfully stopped (Code: 353756)');
                } else {
                    makeErrorAnswer(rpc, 'Could not stop container (Code: 59380934)');

                }
            }   else {
                makeErrorAnswer(rpc, 'Container not found (Code: 45456455445)');
            }
        } else {
            makeErrorAnswer(rpc, 'No device ip or or no container name specified (Code: 909589353)');
        }
    },
    start_container: function(rpc, params) {
        // Will start a container
        if(params['deviceIp'] && typeof params['deviceIp'] === 'string' &&
            params['containerName'] && typeof params['containerName'] === 'string') {
            var device = deviceCtrl.getDeviceByIp(params['deviceIp']),
                container = null;

            if (device && (container = deviceCtrl.getContainerByName(device, params['containerName']))) {
                if (deviceCtrl.startContainer(container, params['user'])) {
                    makeSuccessAnswer(rpc, 'Successfully started (Code: 5464445)');
                } else {
                    makeErrorAnswer(rpc, 'Could not start container (Code: 56734543)');
                }
            } else {
                makeErrorAnswer(rpc, 'Container not found (Code: 345345346767)');
            }

        } else {
            makeErrorAnswer(rpc, 'No device ip or or no container name specified (Code: 345345345)');
        }
    },
    add_device: function (rpc, params) {
        // Will add a device with given IP
        if(
            params['deviceIp'] && typeof params['deviceIp'] === 'string' &&
            params['name'] && typeof params['name'] === 'string'
        ) {
            var ip = params['deviceIp'];
            if(/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ip)) {
                if (deviceCtrl.addDevice(ip, params['name'], params['user'])) {
                    makeSuccessAnswer(rpc, "Device successfully added (Code: 23948290)");
                } else {
                    makeErrorAnswer(rpc, "Could not add device. Maybe this IP is already registered (Code: 3924823904)");
                }
            } else {
                makeErrorAnswer(rpc, "This does not look like a valid IP (Code: 239482)");
            }
        } else {
            makeErrorAnswer(rpc, 'no device ip or no name given (Code: 2394823459)');
        }
    }
};