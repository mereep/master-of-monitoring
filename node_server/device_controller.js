/**
 * Description:
 * Device controller and model to work with the status representation 
 *
 * This file is part of submission
 * @author Richard Vogel <webdes87@gmail.com>
 */

var staticData = require('../js/static_data'),
    _ = require('underscore')
;

module.exports = new function DeviceController () {
    // Including device model ;)
    var devices = [].concat(staticData.startContainers);

    /**
     * Returns a device by name or undefined if nothing to return
     * @returns {Object|undefined}
     */
    this.getDeviceByName = function (name) {
        return _.find(devices, function (elem) {
            return elem.name === name;
        });
    };
    /**
     * Returns a device by ip or undefined if nothing to return
     * @returns {Object|undefined}
     */
    this.getDeviceByIp = function (ip) {
        return _.find(devices, function (elem) {
            return elem.ip === ip;
        });
    };

    /**
     * Returns the devices as ref
     * @return {Object}
     */
    this.getAllDevices = function () {
        return devices;
    };

    /**
     * Gets a container by its name
     *
     * @param {Array} device
     * @param {string} containerName
     * @return {Object|undefined}
     */
    this.getContainerByName = function (device, containerName) {
        return _.find(device.containers, function (container) {
            return container.name === containerName;
        })
    };

    /**
     * Adds a container specified by containerIp
     * @param ip
     * @param {string} containerName
     * @param {string} user
     * @return {boolean}
     */
    this.addContainer = function(ip, containerName, user) {
        // Check if device exists but no container with same name
        var device = this.getDeviceByIp(ip);
        if (!device || typeof this.getContainerByName(device, containerName) !== 'undefined') return false;
        var newContainer = {
            startedBy: user,
            name: containerName,
            state: staticData.states.container_off,
            startTime: new Date()
        };

        device.containers.push(newContainer);
        return true;
    };

    /**
     * Deletes a container
     *
     * @param {string} deviceIp
     * @param {string} containerName
     * @return {boolean}
     */
    this.deleteContainer = function (deviceIp, containerName) {
        var device = this.getDeviceByIp(deviceIp);
        if (device) {
            var container = this.getContainerByName(device, containerName);
            if (container) {
                // Find container index to splice it later on
                var pos = 0;
                _.find(device.containers, function(container) {
                    pos++;
                    return container.name === containerName;
                });

                device.containers = device.containers.splice(pos);
                return true;
            }
        }

        return false;
    };

    /**
     * Starts the container
     * 
     * @param container
     * @param user
     * @returns {boolean}
     */
    this.startContainer = function (container, user) {
        container.state = staticData.states.container_on;
        container.startTime = new Date();
        container.startedBy = user;
        return true;
    };

    /**
     * Stops the container
     *
     * @param container
     * @return {boolean}
     */
    this.stopContainer = function (container) {
        container.state = staticData.states.container_off;
        return true;
    };

    /**
     * Turns a device on
     *
     * @param device
     */
    this.turnOnDevice = function (device, user) {
        device.state = staticData.states.device_online;
        device.startTime = new Date();
        device.startedBy = user;

        var self = this;
        //turn on all containers
        _.map(device.containers, function (container) {
            // Initial state should be off
            self.stopContainer(container);
        })

    };

    /**
     * Turns a device off
     *
     * @param device
     */
    this.turnOffDevice = function (device) {
        device.state = staticData.states.device_offline;

        var self = this;
        //turn off all containers
        _.map(device.containers, function (container) {
            self.stopContainer(container);
        })
    };

    /**
     * Reboots the device (Turning off, setting reboot state, waiting, turning on)
     *
     * @param deviceIp
     * @param user
     */
    this.rebootDevice = function (deviceIp, user) {
        var device = this.getDeviceByIp(deviceIp);

        if(device) {
            this.turnOffDevice(device);
            device.state = staticData.states.device_rebooting;

            var self = this;
            // Simulate reboot
            setTimeout(function() {
                // Check if device still exists
                var device = self.getDeviceByIp(deviceIp);
                if (device) {
                    self.turnOnDevice(device, user);
                }
            }, 5000);

            return true;
        }

        return false;
    };

    /**
     * Adds a new device to the list
     * Will NOT check for valid IP
     * 
     * @param {string} deviceIp
     * @param {string} user
     * @param {string} name
     * @return {boolean}
     */
    this.addDevice = function (deviceIp, name, user) {
        var device = this.getDeviceByIp(deviceIp);
        
        if(device) return false;
        var newDevice = {
            'ip': deviceIp,
            'startedBy': user,
            'name': name,
            'startTime': new Date(),
            'containers': [],
            'state': staticData.states.device_offline
        };

        devices.push(newDevice);

        return true;
    };

    /**
     * Gets a list of all containers
     *
     * @return {Array}
     */
    this.getAllContainers = function () {
        var containers = [];
        _.forEach(this.getAllDevices(), function (device) {
            containers.append(device.containers);
        });

        return containers;
    }
};